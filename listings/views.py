from django.shortcuts import render, get_object_or_404, redirect
from .models import *


# Create your views here.
def home(requests):
    movies = Movie.objects.all().order_by('-id')
    context = {
        'movies': movies
    }

    return render(requests, "listings/index.html", context)

def movie(requests, pk):
    movie = get_object_or_404(Movie, id=pk)
    reviews = Review.objects.all().filter(movie=movie)
    context = {
        'movie': movie,
        'reviews': reviews
    }
    return render(requests, "listings/movie.html", context)

def add_review(requests, movie):
    if requests.method == 'POST':
        review = requests.POST['review']
        type_ = int(requests.POST['type'])
        if type_==1:
            type_ = "positive"
        else:
            type_ = "negative"

        new_review = Review()
        movie_id = get_object_or_404(Movie, id=movie)
        new_review.movie = movie_id
        new_review.type_of_review = type_
        new_review.review = review
        new_review.save()
        return redirect("movie", movie)

