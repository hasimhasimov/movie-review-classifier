from django.db import models


# Create your models here.
class Movie(models.Model):
    title = models.CharField(max_length=255)
    description = models.CharField(max_length=255, null=True)
    image = models.ImageField(upload_to="media/movies/%Y/%m/%d")

    def __str__(self):
        return self.title

    def get_reviews(self):
        reviews = Review.objects.all().filter(movie=self.id)
        return reviews

    def get_reviews_count(self):
        reviews_count = Review.objects.all().filter(movie=self.id).count()
        return reviews_count

    def get_average_point(self):
        try:
            reviews_count = Review.objects.all().filter(movie=self.id, type_of_review="positive").count()
            all_counts = Review.objects.all().filter(movie=self.id).count()


            return round((reviews_count/all_counts)*10, 2)
        except:
            return 0



class Review(models.Model):
    REVIEW_TYPE = (
        ('positive', 'Positive'),
        ('negative', 'Negative')
    )
    review = models.CharField(max_length=255)
    type_of_review = models.CharField(max_length=20, choices=REVIEW_TYPE)  # 1 is Positive, 0 is Negative
    created_at = models.DateTimeField(auto_now_add=True)
    movie = models.ForeignKey(Movie, on_delete=models.CASCADE)

    def __str__(self):
        return self.review
